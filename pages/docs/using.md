# Using the Coverage Generator in your project

## Include in your build setup

Add/Merge the following snippet in your `build.gradle`.

**Note**: List all grammar files in the arguments, and the changed name of JastAdd's list class, if applicable.

```groovy
repositories {
  mavenCentral()
  maven {
    name "gitlab-maven"
    url "https://git-st.inf.tu-dresden.de/api/v4/groups/jastadd/-/packages/maven"
  }
}

configurations {
  coverageGeneratorClasspath
}

dependencies {
  coverageGeneratorClasspath group: 'de.tudresden.inf.st', name: 'coverageGenerator', version: '0.0.4'
}

task generateCoverage(type: JavaExec) {
  main = 'org.jastadd.preprocessor.coverage_gen.Main'
  classpath = configurations.coverageGeneratorClasspath
  
  args([
      '--jastAddList=JastAddList',
      '--outputBaseDir=src/gen/jastadd',
      'src/main/jastadd/Example.relast' 
  ])
}
generateAst.dependsOn generateCoverage
```

## Write a testcase

Create a new test class with the following content.

**Note**: Import the `ASTNode` using your matching package, as well as `@Test` using the correct test framework.

```java
public class GenerateCoverageTest {

  @Test
  public void testGeneratedCoverage() {
    ASTNode.runCoverageAll();
  }
}
```
