coverageGenVersionFileName = '../src/main/resources/coverageGeneratorVersion.properties'


def get_version():
    with open(coverageGenVersionFileName) as coverageGenVersionFile:
        versionFileContent = coverageGenVersionFile.read()
    return versionFileContent[versionFileContent.rindex('version=') + 8:].strip()


def define_env(env):
    """
    This is the hook for defining variables, macros and filters

    - variables: the dictionary that contains the environment variables
    - macro: a decorator function, to declare a macro.
    """
    env.conf['site_name'] = 'Coverage Generator ' + get_version()

    @env.macro
    def coverageGen_version():
        return get_version()


if __name__ == '__main__':
    print(get_version())
