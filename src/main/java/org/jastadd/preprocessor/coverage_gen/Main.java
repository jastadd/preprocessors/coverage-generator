package org.jastadd.preprocessor.coverage_gen;

import org.jastadd.option.BooleanOption;
import org.jastadd.relast.ast.Document;
import org.jastadd.relast.ast.Program;
import org.jastadd.relast.compiler.CompilerException;

import java.io.IOException;
import java.nio.file.Path;

import static org.jastadd.relast.compiler.Mustache.javaMustache;

public class Main extends org.jastadd.relast.compiler.RelAstProcessor {

  private static final String MUSTACHE_TEMPLATE = "Coverage";

  protected BooleanOption optionPrintYaml;

  public Main() {
    super("Coverage Generator", true);
  }

  public static void main(String[] args) {
    try {
      int returnValue = new Main().run(args);
      System.exit(returnValue);
    } catch (CompilerException e) {
      System.err.println(e.getMessage());
      System.exit(-1);
    }
  }

  @Override
  protected void initOptions() {
    super.initOptions();
    optionPrintYaml = addOption(new BooleanOption("printYaml", "create YAML file used by Mustache in the ouput directory"));
    optionPrintYaml.defaultValue(false);
  }

  @Override
  protected int processGrammar(Program program, Path inputBasePath, Path outputBasePath) throws CompilerException {

    Document navigation = program.toCoverage(getConfiguration().listType());

    Path yamlFile = outputBasePath.resolve(navigation.getFileName());
    Path jragFile = yamlFile.getParent().resolve(MUSTACHE_TEMPLATE + ".jrag");

    if (optionPrintYaml.value() != null && optionPrintYaml.value()) {
      writeToFile(yamlFile, navigation.prettyPrint(false));
    }

    try {
      javaMustache(MUSTACHE_TEMPLATE, navigation.prettyPrint(), jragFile.toString());
    } catch (IOException e) {
      throw new CompilerException("Unable to expand template", e);
    }

    return 0;
  }

}
