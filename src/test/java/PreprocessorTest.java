import org.jastadd.preprocessor.coverage_gen.Main;
import org.jastadd.relast.tests.RelAstProcessorTestBase;
import org.junit.jupiter.api.BeforeAll;

public class PreprocessorTest extends RelAstProcessorTestBase {
  @BeforeAll
  static void init() {
    mainClass = Main.class;
  }
}
